import React, { useState, useEffect } from "react";
import PostDataService from "./services/posts.service";
import { Link } from "react-router-dom";
import ReactQuill from "react-quill";
import quillConfig from "./config/quill.config";
import "react-quill/dist/quill.snow.css";
import { useHistory } from "react-router-dom";

const AddArticle = () => {
  let history = useHistory();

  const [body, setBody] = useState("");

  const [title, setTitle] = useState("");

  useEffect(() => {
    localStorage.getItem('token')===null && history.replace('/auth')
  }, []);

  const addPost = () => {
    var data = {
      title: title,
      body: body,
    };

    PostDataService.create(data)
      .then((response) => {
        console.log(response.data);
        history.push("/article");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div class="md:w-4/5 md:mx-auto m-5">
      <div class="heading font-bold text-2xl mb-5 text-gray-800">New Post</div>
      <div class="bg-white editor mx-auto flex flex-col text-gray-800 border border-gray-300 p-4 shadow-lg">
        <input
          class="title border border-gray-300 p-2 mb-4 outline-none"
          spellcheck="false"
          placeholder="Title"
          type="text"
          onChange={(e) => setTitle(e.target.value)}
        />
        <ReactQuill
          theme="snow"
          modules={quillConfig.modules}
          onChange={setBody}
        />

        <div class="buttons ml-auto mt-5 flex">
          <div
            class="btn border border-indigo-500 p-1 px-4 font-semibold cursor-pointer text-gray-200 ml-2 bg-indigo-500"
            onClick={() => addPost()}
          >
            Post
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddArticle;
