import React, { useState, useEffect } from "react";
import PostDataService from "./services/posts.service";
import { Link } from "react-router-dom";
import Truncate from "react-truncate";

const PostsList = () => {
  const [Posts, setPosts] = useState(null);
  const [currentPost, setCurrentPost] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);
  const [searchTitle, setSearchTitle] = useState("");

  useEffect(() => {
    retrievePosts();
  }, []);

  const retrievePosts = () => {
    PostDataService.getAll()
      .then((response) => {
        setPosts(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const refreshList = () => {
    retrievePosts();
    setCurrentPost(null);
    setCurrentIndex(-1);
  };

  const setActivePost = (Post, index) => {
    setCurrentPost(Post);
    setCurrentIndex(index);
  };

  const removeAllPosts = () => {
    PostDataService.removeAll()
      .then((response) => {
        console.log(response.data);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const findByTitle = (title) => {
    setPosts(null);
    PostDataService.findByTitle(title)
      .then((response) => {
        setPosts(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div class="md:mx-auto md:w-4/5">
      <div class="text-gray-600 mb-5">
        <input
          type="search"
          name="serch"
          placeholder="Search"
          class="bg-white w-full h-10 px-5 pr-10 rounded-full text-sm focus:outline-none rounded-sm shadow-md"
          onChange={(e) => findByTitle(e.target.value)}
        />
      </div>
      {Posts ? (
        Posts.map((post, index) => (
          <div class="w-full px-10 my-4 py-6 bg-white rounded-lg shadow-md">
            <div class="flex justify-between items-center">
              <span class="font-light text-xs text-gray-600">
                <i class="far fa-clock"></i>{" "}
                {new Date(post.createdAt).toLocaleDateString("id-ID")}
              </span>
              {localStorage.getItem("token") && (
                <Link
                  class="px-2 py-1 bg-red-600 text-white font-bold rounded hover:bg-red-500"
                  to={`/article/edit/${post.id}`}
                >
                  Edit{" "}
                </Link>
              )}
            </div>
            <div class="mt-2">
              <a
                class="text-2xl text-gray-700 font-bold hover:text-gray-600"
                href="#"
              >
                {post.title}
              </a>
              <p class="mt-2 text-gray-600">
                <Truncate lines={3} ellipsis={<span>...</span>}>
                  <td dangerouslySetInnerHTML={{ __html: post.body }} />
                </Truncate>
              </p>
            </div>
            <div class="flex justify-between items-center mt-4">
              <Link
                class="text-blue-600 hover:underline"
                to={`/article/detail/${post.id}`}
              >
                Read more{" "}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  class="w-4 h-4 inline-block"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M17 8l4 4m0 0l-4 4m4-4H3"
                  />
                </svg>
              </Link>
              <div>
                <a class="flex items-center" href="#">
                  <img
                    class="mx-4 w-10 h-10 object-cover rounded-full hidden sm:block"
                    src="/harun.jpg"
                    alt="avatar"
                  />
                  <h1 class="text-gray-700 font-bold">Harun Hasibuan</h1>
                </a>
              </div>
            </div>
          </div>
        ))
      ) : (
        <div class="w-full h-full block top-0 left-0 opacity-75 z-50">
          <span
            class="text-green-500 top-1/2 my-0 mx-auto block relative w-20 h-20"
            style={{ top: "50%" }}
          >
            <i class="fas fa-circle-notch fa-spin fa-3x"></i>
          </span>
        </div>
      )}
    </div>
  );
};

export default PostsList;
