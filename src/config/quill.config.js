export default {
    modules: {
      toolbar: [
        [{ 'header': [1, 2, 3, 4, false] }],
        [{'font': []}, {'color': []}, {'background': []}],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{ align: '' }, { align: 'center' }, { align: 'right' }, { align: 'justify' }],
        [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
        ['link', 'image'],
        ['clean']
      ],
    }
  }
  