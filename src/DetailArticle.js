import React, { useState, useEffect } from "react";
import PostDataService from "./services/posts.service";
import { Link } from "react-router-dom";
import { useHistory, useParams } from "react-router-dom";

const DetailArticle = () => {
  let history = useHistory();

  const { id } = useParams();

  const [currentArticle, setCurrentArticle] = useState(null);

  useEffect(() => {
    getPost(id);
  }, []);

  const getPost = (id) => {
    PostDataService.get(id)
      .then((response) => {
        setCurrentArticle(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return currentArticle ? (
    <div class="md:w-4/5 md:mx-auto m-5">
      <nav class="text-black font-bold my-8" aria-label="Breadcrumb">
        <ol class="list-none p-0 inline-flex">
          <li class="flex items-center">
            <Link to="/">Home</Link>
            <svg
              class="fill-current w-3 h-3 mx-3"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 320 512"
            >
              <path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
            </svg>
          </li>
          <li class="flex items-center">
            <Link to="/article">Article</Link>
            <svg
              class="fill-current w-3 h-3 mx-3"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 320 512"
            >
              <path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
            </svg>
          </li>
          <li>
            <span class="text-gray-500" aria-current="page">
              {currentArticle.title}
            </span>
          </li>
        </ol>
      </nav>
      <div class="heading font-bold text-2xl mb-5 text-gray-800">
        {currentArticle.title}
      </div>
      <div class="heading mb-5 text-gray-600">
        Published on{" "}
        {new Date(currentArticle.createdAt).toLocaleDateString("id-ID")}
      </div>
      <div class="editor bg-white mx-auto flex flex-col text-gray-800 p-4 shadow-lg">
        <td dangerouslySetInnerHTML={{ __html: currentArticle.body }} />
      </div>
    </div>
  ) : (
    <div class="w-full h-full block top-0 left-0 opacity-75 z-50">
      <span
        class="text-green-500 top-1/2 my-0 mx-auto block relative w-20 h-20"
        style={{ top: "50%" }}
      >
        <i class="fas fa-circle-notch fa-spin fa-3x"></i>
      </span>
    </div>
  );
};

export default DetailArticle;
