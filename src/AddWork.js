import React, { useState, useEffect } from "react";
import PostDataService from "./services/posts.service";
import { Link } from "react-router-dom";
import ReactQuill from "react-quill";
import quillConfig from "./config/quill.config";
import "react-quill/dist/quill.snow.css";
import { useHistory } from "react-router-dom";

const AddWork = () => {
  let history = useHistory();

  const [body, setBody] = useState("");
  const [title, setTitle] = useState("");
  const [stack, setStack] = useState("");
  const [url, setURL] = useState("");
  const [file, setFile] = useState();

  useEffect(() => {
    localStorage.getItem("token") === null && history.replace("/auth");
  }, []);

  const addWork = () => {
    let data = new FormData();
    data.append("file", file);
    data.append("title", title);
    data.append("stack", stack);
    data.append("url", url);
    data.append("body", body);

    PostDataService.createWork(data)
      .then((response) => {
        console.log(response.data);
        history.push("/work");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div class="md:w-4/5 md:mx-auto m-5">
      <div class="heading font-bold text-2xl mb-5 text-gray-800">New Work</div>
      <div class="bg-white editor mx-auto flex flex-col text-gray-800 border border-gray-300 p-4 shadow-lg">
        <input
          class="title border border-gray-300 p-2 mb-4 outline-none"
          spellcheck="false"
          placeholder="Work Name"
          type="text"
          name="title"
          onChange={(e) => setTitle(e.target.value)}
        />
        <input
          class="title border border-gray-300 p-2 mb-4 outline-none"
          spellcheck="false"
          placeholder="Description"
          type="text"
          name="body"
          onChange={(e) => setBody(e.target.value)}
        />
        <input
          class="title border border-gray-300 p-2 mb-4 outline-none"
          spellcheck="false"
          placeholder="Work Stack"
          type="text"
          name="stack"
          onChange={(e) => setStack(e.target.value)}
        />
        <input
          class="title border border-gray-300 p-2 mb-4 outline-none"
          spellcheck="false"
          placeholder="Work URL"
          type="text"
          name="url"
          onChange={(e) => setURL(e.target.value)}
        />
        <input
          class="title border border-gray-300 p-2 mb-4 outline-none"
          spellcheck="false"
          placeholder="Work Image"
          type="file"
          name="file"
          onChange={(e) => setFile(e.target.files[0])}
        />
        <div class="buttons ml-auto mt-5 flex">
          <button
            onClick={()=>addWork()}
            class="btn border border-indigo-500 p-1 px-4 font-semibold cursor-pointer text-gray-200 ml-2 bg-indigo-500"
          >
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};

export default AddWork;
