import React from "react";
import { Link } from "react-router-dom";

function Home() {
  return (
    <div class="col-span-1 md:flex items-center m-10 justify-center">
      {/* <div class="md:mr-4 md:block sm:hidden xs:hidden">
        <img
          class="md:h-40 sm:h-40 xs:h-20 rounded-full self-center"
          src="/harun.jpg"
          alt=""
        />
      </div> */}
      <div class="pl-4 p-2 col-span-2 md:text-justify md:w-1/2 mt-10 md:mt-0 sm:text-center xs:text-center">
        <h1 class="mt-4 mb-3 text-2xl">Hi, i'm Harun, </h1>
        <p>
          this playground still under development. But, fell free to read my own{" "}
          <Link
            class="text-sm shadow-lg px-3 bg-gray-200 hover:bg-gray-300 rounded-full"
            to="/article"
          >
            article
          </Link>{" "}
          or{" "}
          <Link
            class="text-sm shadow-lg px-3 bg-gray-200 hover:bg-gray-300 rounded-full"
            to="/work"
          >
            work
          </Link>
        </p>
        <div class="mt-5 text-2xl">
          <a
            target="_blank"
            href="https://www.linkedin.com/in/harun-hasibuan-38885a134/"
          >
            <i class="fab fa-linkedin"></i>
          </a>{" "}
          <a target="_blank" href="https://twitter.com/harunhsb">
            <i class="fab fa-twitter-square"></i>
          </a>
        </div>
      </div>
    </div>
  );
}

export default Home;
