import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.css";
import Home from "./Home";
import Article from "./ArticleList";
import AddArticle from "./AddArticle";
import DetailArticle from "./DetailArticle";
import Login from "./Login";
import PostDataService from "./services/posts.service";
import { useHistory } from "react-router-dom";
import AddWork from "./AddWork";
import WorksList from "./WorkList";
import EditArticle from "./EditArticle";

function App() {
  let history = useHistory();

  const logout = () => {
    var data = {
      token: localStorage.getItem("refreshToken"),
    };

    PostDataService.logout(data)
      .then((response) => {
        console.log(response.data);
        localStorage.clear();
        window.location.replace("/auth");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <Router>
      <nav class="md:w-4/5 sm:px-10 md:px-0 xs:px-10 py-10 flex relative justify-between items-center mx-auto h-20">
        <div class="inline-flex">
          <Link class="_o6689fn" to="/">
            <div class="md:block">
              <h1 class="text-xl ml-2">Harun Hasibuan</h1>
            </div>
          </Link>
        </div>

        <div class="flex-initial">
          <div class="flex justify-end items-center relative">
            <div class="flex mr-4 items-center">
              <Link
                class="inline-block shadow-lg py-2 px-3 bg-gray-200 hover:bg-gray-300 rounded-full mr-1"
                to="/work"
              >
                <div class="flex items-center relative cursor-pointer whitespace-nowrap">
                  work
                </div>
              </Link>
              <Link
                class="inline-block shadow-lg py-2 px-3 bg-gray-200 hover:bg-gray-300 rounded-full mr-1"
                to="/article"
              >
                <div class="flex items-center relative cursor-pointer whitespace-nowrap">
                  article
                </div>
              </Link>
              <Link
                class="inline-block shadow-lg py-2 px-3 bg-gray-200 hover:bg-gray-300 rounded-full mr-1"
                to="/article/detail/60478e2752997e0015d41e7c"
              >
                <div class="flex items-center relative cursor-pointer whitespace-nowrap">
                  about
                </div>
              </Link>
              <div class="group inline-block">
                <button class="rounded-full shadow-lg bg-blue-300 p-2 text-white flex items-center">
                  <span class="pr-1 font-semibold flex-1">
                    <i class="fas fa-bars"></i>
                  </span>
                  <span>
                    <svg
                      class="fill-current h-4 w-4 transform group-hover:-rotate-180
        transition duration-150 ease-in-out"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                    >
                      <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                  </span>
                </button>
                <ul
                  class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute 
  transition duration-150 ease-in-out origin-top min-w-32"
                >
                  {localStorage.getItem("token") == null ? (
                    <Link to="/auth">
                      <li class="rounded-sm px-3 py-1 hover:bg-gray-100">
                        Login <i class="fas fa-sign-in-alt"></i>
                      </li>
                    </Link>
                  ) : (
                    <>
                      <Link to="/article/add">
                        <li class="rounded-sm px-3 py-1 hover:bg-gray-100">
                          New Article <i class="fas fa-plus-square"></i>
                        </li>
                      </Link>
                      <Link to="/work/add">
                        <li class="rounded-sm px-3 py-1 hover:bg-gray-100">
                          New Work <i class="fas fa-plus-square"></i>
                        </li>
                      </Link>
                      <li
                        class="rounded-sm px-3 py-1 hover:bg-gray-100"
                        onClick={() => logout()}
                      >
                        Logout <i class="fas fa-sign-in-alt"></i>
                      </li>
                    </>
                  )}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </nav>

      <Switch>
        <Route path="/auth">
          <Login />
        </Route>
        <Route
          path="/article"
          render={({ match: { url } }) => (
            <>
              <Route path={`${url}/`} component={Article} exact />
              <Route path={`${url}/add`} component={AddArticle} />
              <Route path={`${url}/detail/:id`} component={DetailArticle} />
              <Route path={`${url}/edit/:id`} component={EditArticle} />
            </>
          )}
        />
        <Route
          path="/work"
          render={({ match: { url } }) => (
            <>
              <Route path={`${url}/`} component={WorksList} exact />
              <Route path={`${url}/add`} component={AddWork} />
            </>
          )}
        />
        <Route path="/">
          <Home />
        </Route>
      </Switch>
      <div class="md:container md:mx-auto flex justify-center items-center relative p-5 text-center">
        <p>made on 2021 with react, tailwind, express.js & mongodb.</p>
      </div>
    </Router>
  );
}

export default App;
