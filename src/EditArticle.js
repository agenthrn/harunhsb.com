import React, { useState, useEffect } from "react";
import PostDataService from "./services/posts.service";
import { Link, useParams } from "react-router-dom";
import ReactQuill from "react-quill";
import quillConfig from "./config/quill.config";
import "react-quill/dist/quill.snow.css";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

const EditArticle = () => {
  const { id } = useParams();

  let history = useHistory();

  const [body, setBody] = useState("");

  const [title, setTitle] = useState("");

  const [currentArticle, setCurrentArticle] = useState(null);

  useEffect(() => {
    localStorage.getItem("token") === null && history.replace("/auth");
    getPost(id);
  }, []);

  const getPost = (id) => {
    PostDataService.get(id)
      .then((response) => {
        setCurrentArticle(response.data);
        setTitle(response.data.title);
        setBody(response.data.body);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const editPost = () => {
    var data = {
      title: title,
      body: body,
    };

    PostDataService.update(id, data)
      .then((response) => {
        console.log(response.data);
        history.push(`/article/detail/${id}`);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const deletePost = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        PostDataService.delete(id)
          .then((response) => {
            console.log(response.data);
            history.push(`/article`);
          })
          .catch((e) => {
            console.log(e);
          });
      }
    });
  };

  return currentArticle ? (
    <div class="md:w-4/5 md:mx-auto m-5">
      <div class="heading font-bold text-2xl mb-5 text-gray-800">Edit Post</div>
      <div class="bg-white editor mx-auto flex flex-col text-gray-800 border border-gray-300 p-4 shadow-lg">
        <input
          class="title border border-gray-300 p-2 mb-4 outline-none"
          spellcheck="false"
          placeholder="Title"
          type="text"
          defaultValue={currentArticle.title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <ReactQuill
          theme="snow"
          defaultValue={currentArticle.body}
          modules={quillConfig.modules}
          onChange={setBody}
        />

        <div class="buttons ml-auto mt-5 flex">
          <div
            class="btn border border-red-500 p-1 px-4 font-semibold cursor-pointer text-white ml-2 bg-red-500"
            onClick={() => deletePost()}
          >
            Delete
          </div>
          <div
            class="btn border border-indigo-500 p-1 px-4 font-semibold cursor-pointer text-gray-200 ml-2 bg-indigo-500"
            onClick={() => editPost()}
          >
            Post
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div class="w-full h-full block top-0 left-0 opacity-75 z-50">
      <span
        class="text-green-500 top-1/2 my-0 mx-auto block relative w-20 h-20"
        style={{ top: "50%" }}
      >
        <i class="fas fa-circle-notch fa-spin fa-3x"></i>
      </span>
    </div>
  );
};

export default EditArticle;
