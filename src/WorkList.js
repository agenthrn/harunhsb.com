import React, { useState, useEffect } from "react";
import PostDataService from "./services/posts.service";

const WorksList = () => {
  const [Works, setWorks] = useState(null);
  const [currentPost, setCurrentPost] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);
  const [searchTitle, setSearchTitle] = useState("");

  useEffect(() => {
    retrieveWorks();
  }, []);

  const STACK = {
    "express.js": (
      <a
        href="#"
        class="inline-block rounded-full text-white 
                              bg-red-400 hover:bg-red-500 duration-300 
                              text-xs font-bold 
                              mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
                              opacity-90 hover:opacity-100"
      >
        Express.Js
      </a>
    ),
    "mongo db": (
      <a
        href="#"
        class="inline-block rounded-full text-white 
                              bg-yellow-400 hover:bg-yellow-500 duration-300 
                              text-xs font-bold 
                              mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
                              opacity-90 hover:opacity-100"
      >
        Mongo DB
      </a>
    ),
    "react": (
      <a
        href="#"
        class="inline-block rounded-full text-white 
              bg-green-400 hover:bg-green-500 duration-300 
              text-xs font-bold 
              mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
              opacity-90 hover:opacity-100"
      >
        React
      </a>
    ),
    "laravel": (
      <a
        href="#"
        class="inline-block rounded-full text-white 
              bg-red-600 hover:bg-red-700 duration-300 
              text-xs font-bold 
              mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
              opacity-90 hover:opacity-100"
      >
        Laravel
      </a>
    ),
    "MySQL": (
      <a
        href="#"
        class="inline-block rounded-full text-white 
              bg-blue-400 hover:bg-blue-500 duration-300 
              text-xs font-bold 
              mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
              opacity-90 hover:opacity-100"
      >
        MySQL
      </a>
    ),
    "PostgreSQL": (
      <a
        href="#"
        class="inline-block rounded-full text-white 
              bg-yellow-600 hover:bg-yellow-700 duration-300 
              text-xs font-bold 
              mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
              opacity-90 hover:opacity-100"
      >
        PostgreSQL
      </a>
    ),
    "bootstrap": (
      <a
        href="#"
        class="inline-block rounded-full text-white 
              bg-blue-700 hover:bg-blue-900 duration-300 
              text-xs font-bold 
              mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
              opacity-90 hover:opacity-100"
      >
        Bootstrap
      </a>
    ),
    'vue.js' : (
      <a
        href="#"
        class="inline-block rounded-full text-white 
              bg-green-300 hover:bg-green-400 duration-300 
              text-xs font-bold 
              mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
              opacity-90 hover:opacity-100"
      >
        VueJs
      </a>
    ),
  };

  const retrieveWorks = () => {
    PostDataService.getAllWorks()
      .then((response) => {
        setWorks(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const refreshList = () => {
    retrieveWorks();
    setCurrentPost(null);
    setCurrentIndex(-1);
  };

  const setActivePost = (Post, index) => {
    setCurrentPost(Post);
    setCurrentIndex(index);
  };

  const removeAllWorks = () => {
    PostDataService.removeAll()
      .then((response) => {
        console.log(response.data);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const findByTitle = (title) => {
    setWorks(null);
    PostDataService.findByTitle(title)
      .then((response) => {
        setWorks(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div class="md:mx-auto md:w-4/5">
      {/* <div class="text-gray-600 mb-5">
        <input
          type="search"
          name="serch"
          placeholder="Search"
          class="bg-white w-full h-10 px-5 pr-10 rounded-full text-sm focus:outline-none rounded-sm shadow-md"
          onChange={(e) => findByTitle(e.target.value)}
        />
      </div> */}
      <h1 class="text-3xl">Recent Works & Project</h1>
      {Works ? (
        Works.map((work, index) => (
          <div class="bg-white rounded-lg shadow-sm hover:shadow-lg duration-500 px-2 sm:px-6 md:px-2 py-4 my-6">
            <div class="grid grid-cols-12 gap-3">
              <div class="col-span-12 sm:col-span-2 items-center my-auto">
                <img
                  src={work.image}
                  class="w-full"
                />
              </div>
              <div class="col-span-12 sm:col-start-3 sm:col-end-13 px-3 sm:px-0">
                <div class="mt-2">
                  <a
                    href={work.url}
                    target="_blank"
                    class="sm:text-sm md:text-md lg:text-lg text-blue-500 font-bold hover:underline"
                  >
                    {work.title} <i class="fas fa-external-link-alt text-xs align-top"></i>
                  </a>

                  <p class="mt-2 text-gray-600 text-sm md:text-md">
                    {work.body}
                  </p>
                </div>
                <div class="grid grid-cols-2 mt-4 my-auto">
                  <div class="col-span-12 lg:col-span-8">
                    {work.stack.split(",").map((stack) => STACK[stack])}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))
      ) : (
        <div class="w-full h-full block top-0 left-0 opacity-75 z-50">
          <span
            class="text-green-500 top-1/2 my-0 mx-auto block relative w-20 h-20"
            style={{ top: "50%" }}
          >
            <i class="fas fa-circle-notch fa-spin fa-3x"></i>
          </span>
        </div>
      )}
    </div>
  );
};

export default WorksList;
