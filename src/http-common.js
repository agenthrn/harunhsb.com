import axios from "axios";

class httpCommon {
  auth(){
    return axios.create({
      baseURL: "https://backend-agenthrn.herokuapp.com/api",
      headers: {
        "Content-type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      },
    });
  }

  noAuth(){
    return axios.create({
      baseURL: "https://backend-agenthrn.herokuapp.com/api",
      headers: {
        "Content-type": "application/json"
      },
    });
    
  }
}

export default new httpCommon();