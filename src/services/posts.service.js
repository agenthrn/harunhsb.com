import http from "../http-common";

class postDataService {

  login(data) {
    return http.noAuth().post("/login", data);
  }

  logout(data) {
    return http.noAuth().post("/logout", data);
  }

  newToken(data) {
    return http.noAuth().post("/token", data);
  }

  getAll() {
    return http.auth().get("/posts");
  }

  getAllWorks() {
    return http.noAuth().get("/posts/work");
  }

  get(id) {
    return http.auth().get(`/posts/${id}`);
  }

  create(data) {
    return http.auth().post("/posts", data);
  }

  createWork(data) {
    return http.auth().post("/posts/work", data);
  }

  update(id, data) {
    return http.auth().put(`/posts/${id}`, data);
  }

  delete(id) {
    return http.auth().delete(`/posts/${id}`);
  }

  deleteAll() {
    return http.auth().delete(`/posts`);
  }

  findByTitle(title) {
    return http.auth().get(`/posts?title=${title}`);
  }
}

export default new postDataService();
